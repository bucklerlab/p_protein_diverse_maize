#!/bin/bash

# Download reads from SRA
fastq-dump --split-3 SRR1763106
fastq-dump --split-3 SRR1763107

# Download reference fasta
wget https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0.fa.gz
gzip -d Zm-B73-REFERENCE-NAM-5.0.fa.gz

# From https://doi.org/10.1111/tpj.13073 - paper these reads come from:
# "Adapter sequence (5′‐CTGTAGGCACCATCAAT‐3′) were trimmed out by fastx_clipper
#  with parameters –Q 33 ‐a CTGTAGGCACCATCAAT ‐l 25 ‐n ‐v, then fastx_trimmer
#  with parameters –Q 33 ‐f 2 was used to trim out the first nucleotide from 5′
#  end of each read for the reason that it frequently represents an untemplated
#  addition during reverse transcription."

# From https://link.springer.com/protocol/10.1007/978-1-4939-7315-6_10
# Ribosome profiling in maize:
# "To accurately determine the size of ribosome footprints, trimming nucleotides
#  with low sequencing quality from the end (typically known as quality
#  trimming) should be avoided because this can artifactually truncate the
#  ribosome footprint length."

# Trim adapters and run fastqc

cutadapt -a CTGTAGGCACCATCAAT -m 20 -j 10 SRR1763106.fastq -o SRR1763106_clipped.fastq
cutadapt -a CTGTAGGCACCATCAAT -m 20 -j 10 SRR1763107.fastq -o SRR1763107_clipped.fastq

# Build hisat2 index
hisat2-build -p 10 Zm-B73-REFERENCE-NAM-5.0.fa Zm-B73-REFERENCE-NAM-5.0

# Align reads:
#  --trim5 1 removes the first base from the read as recommended by authors
hisat2 -x Zm-B73-REFERENCE-NAM-5.0 \
  --trim5 1 \
  -p 10 \ # threads
  -U SRR1763106_clipped.fastq \
  -S SRR1763106_clipped.sam

hisat2 -x Zm-B73-REFERENCE-NAM-5.0 \
  -t \
  --trim5 1 \
  -p 10 \ # threads
  -U SRR1763107_clipped.fastq \
  -S SRR1763107_clipped.sam

# Sort and index alignments
mkdir -p bam

grep  -E '^@|NH:i:1' SRR1763106_clipped.sam | \
samtools sort - -@ 10 -o bam/SRR1763106_unique.bam
samtools index -@ 10 bam/SRR1763106_unique.bam

grep  -E '^@|NH:i:1' SRR1763107_clipped.sam | \
samtools sort - -@ 10 -o bam/SRR1763107_unique.bam
samtools index -@ 10 bam/SRR1763107_unique.bam

##########################
# Map mRNA reads as well
fastq-dump --split-3 SRR1765338
fastq-dump --split-3 SRR1765337

hisat2 -x Zm-B73-REFERENCE-NAM-5.0 \
  -p 10 \
  --max-intronlen 50000 \
  -1 SRR1765338_1.fastq -2 SRR1765338_2.fastq \
  -S SRR1765338.sam
  
hisat2 -x Zm-B73-REFERENCE-NAM-5.0 \
  -p 10 \
  --max-intronlen 50000 \
  -1 SRR1765337_1.fastq -2 SRR1765337_2.fastq \
  -S SRR1765337.sam
  
grep  -E '^@|NH:i:1' SRR1765338.sam | \
samtools sort - -@ 10 -o bam/SRR1765338_unique.bam
samtools index -@ 10 bam/SRR1765338_unique.bam

grep  -E '^@|NH:i:1' SRR1765337.sam | \
samtools sort - -@ 10 -o bam/SRR1765337_unique.bam
samtools index -@ 10 bam/SRR1765337_unique.bam
  

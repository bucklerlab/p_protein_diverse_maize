
cd ~/projects/p_protein_diverse_maize/data

# Reference fasta
wget https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0.fa.gz
gzip -d Zm-B73-REFERENCE-NAM-5.0.fa.gz

# Proteome
wget https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.protein.fa.gz
gzip -d Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.protein.fa.gz

# GFF
wget https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3.gz
gzip -d Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3.gz

# Make GFF of only canonical transcripts

# Will use canonical_transcripts.txt to search for lines from the original gff to keep.
#  We want all lines with "logic_name=cshl_gene" because those are the main gene entries
echo "logic_name=cshl_gene" > canonical_transcripts.txt
# Find all canonical transcripts and add them to the search file.
grep "canonical_transcript=1" Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3 | \
  awk -F'[:;]' '{print $4}' |
  awk -F'=' '{print $2}' >> canonical_transcripts.txt

# Copy the header into the new GFF
grep -F "#" Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3 > Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.canonical.gff3
# Find all the gene entries and entries containing canonical transcript IDs
grep -f canonical_transcripts.txt Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3 >> Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.canonical.gff3


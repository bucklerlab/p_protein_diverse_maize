# Joe Gage

# Setup -------------------------------------------------------------------
setwd("~/projects/p_protein_diverse_maize/")

# * Import packages ----
library(tidyverse)
library(magrittr)
library(patchwork)
library(GenomicRanges)
library(GenomicFeatures)
library(SummarizedExperiment)
library(ORFik)
library(DiffLogo)
library(ggseqlogo)

# * Set path variables ----
rare_path = "data/rare_variants_mrna.txt"
abundance_path = "data/Jiang2019_AllGenos_mRNA.txt"

gff_path = "data/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3"
ref_path = "data/Zm-B73-REFERENCE-NAM-5.0.fa"


# Files for gene name conversions
# orig_data_path = "data/convert_Jiang2019_v5/Jiang2019_139955_1_supp_230491_phrhk9.txt"
# uniparc_id_path = "data/convert_Jiang2019_v5/Jiang2019_Uniparc_IDs.txt"
# blast_results_path = "data/convert_Jiang2019_v5/Jiang2019_Uniprot_sequences_v5.blastp"

# Biomart orthologs and putative functions
# ortho_path = "data/biomart_orthologs.txt"
# function_path = "data/biomart_function.txt"

# * Set parameter variables ----
MAF_limit = 0.02


# Load & format data ---------------------------------------------------------------

rare = read_tsv(rare_path) %>%
  mutate(Feature = replace(Feature, Feature == "Intron", "Introns"))

if(!dir.exists("figs")){
  dir.create("figs")
}
if(!dir.exists("figs/supplemental")){
  dir.create("figs/supplemental")
}
# * Prepare data ----------------------------------------------------------
# Summarise & count the number of rare variants an individual has 
#  in a particular feature of a particular gene.
rarect = rare %>%
  filter(MAF <= MAF_limit) %>%
  group_by(Inbred, Gene, Feature) %>%
  summarise(N = sum(has_minor_allele),
            Abundance = mRNA[1],
            Any_rare = N > 0) %>%
  ungroup() 

# Get data for the null distribution: individuals with NO rare alleles in any of
#  the genomic features. Same null for all features.
abundance_ind = read_tsv(abundance_path)
no_rare = anti_join(abundance_ind,
                    rarect %>% filter(Any_rare),
                    by=c("Gene", "Inbred")) %>%
  group_by(Gene) %>%
  summarise(Abundance = log2( (2^mRNA) / 2^median(mRNA) ),
            Inbred = Inbred) %>%
  ungroup()

# Test and plot signed value of protein abundance ---------------------------

# * Perform significance tests ------
# Make dataframe to hold pvals
sig = rarect %>% 
  ungroup %>%
  filter(Any_rare) %>%
  group_by(Feature) %>% 
  summarise(nObs = n(),
            nGene = length(unique(Gene)),
            nNULL=nrow(no_rare)) %>%
  mutate(p_ks=1, p_w=1, perm=1, KS_D = 0, p_fvar=1)

# Calculate pvals for each feature seperately
# * * KS and Wilcox tests --------------------------------------------------
for(f in unique(rarect$Feature)){
  x = rarect %>% filter(Feature == f, Any_rare == TRUE) %>% pull(Abundance)

  y = no_rare$Abundance

  K = ks.test(x, y, alternative="two.sided")
  W = wilcox.test(x, y, alternative="two.sided")
  Fvar = var.test(x, y, alternative = "greater")
  
  sig[sig$Feature == f, "p_w"] <- W$p.value
  sig[sig$Feature == f, "p_ks"] <- K$p.value
  sig[sig$Feature == f, "KS_D"] <- K$statistic
  sig[sig$Feature == f, "p_fvar"] <- Fvar$p.value
  
}

# * * Permutation tests -------------------------------------------------------
# Permutation-based significance test for difference of mean
# perms = matrix(NA, nrow=n_perm, ncol=length(sig$Feature))
# colnames(perms) = sig2$Feature
# # For some reason doing this with %dopar% does not always return permutations for all the features
# for(f in sig2$Feature){
#   print(sprintf("Permuting %s", f))
#   x = rarect %>% filter(Feature == f, Any_rare == TRUE) %>% pull(Abundance)
#   y = no_rare$Abundance
#   
#   orig_abundance = c(x, y)
#   labels = c(rep(1, length(x)), # 1 == Has rare variant(s)
#              rep(0, length(y))) # 0 == No rare variants
#   perms[,f] = sapply(1:n_perm, function(i){
#     perm_labels = sample(labels, length(labels), replace = FALSE)
#     mean(orig_abundance[perm_labels == 0]) - mean(orig_abundance[perm_labels == 1])
#   })
# }
# # Calculate significance by comparing the real value to the permutations
# sig2$perm = sapply(colnames(perms), function(f){
#   x = rarect %>% filter(Feature == f, Any_rare == TRUE) %>% pull(Abundance)
#   y = no_rare$Abundance
#   mean(abs(perms[,f]) > abs(mean(x)-mean(y))) # Compare abs values for 2-sided test
# })

# * Density plot -----------------------------------------------------------
# Plot densities of abundance for inds with rare variants vs null
bw = "nrd0"
adj = 1

rarect$Feature_factor = factor(rarect$Feature, levels=c("Promoter", "5' UTR", "Introns", "CDS", "3' UTR"))
sig$Feature_factor = factor(sig$Feature, levels=c("Promoter", "5' UTR", "Introns", "CDS", "3' UTR"))
all_density_fig = ggplot(mapping=aes(Abundance, y=..density..)) +
  geom_density(alpha=0.2, mapping=aes(color="No", fill="No"), data=no_rare, bw=bw, adjust=adj, n=1024) +
  geom_density(alpha=0.2, mapping=aes(color="Yes", fill="Yes"), data=rarect %>% filter(Any_rare, Feature != "Transcript"), adjust=adj, bw=bw, n=1024) +
  facet_wrap(~Feature_factor, nrow=1) +
  coord_cartesian(xlim=c(-1.5, 1.5)) +
  # labs(title=sprintf("MAF < %0.3f", MAF_limit)) +
  labs(x="mRNA Abundance", y="Density") +
  geom_text(aes(label=sprintf("p = %1.2e \nn Rare = %i ", p_ks, nObs)), fontface="italic", color="gray30", size=3,
            x=Inf, y=Inf,data=sig %>% filter(Feature != "Transcript"), inherit.aes=FALSE, hjust=1, vjust=1.1) + 
  theme_classic() +
  theme(strip.text.x = element_text(size = 12)) +
  scale_fill_manual(name="Gene has\nrare allele", values=c("No"="gray", "Yes"="red")) +
  scale_color_manual(name="Gene has\nrare allele", values=c("No"="gray", "Yes"="red")) +
  theme(legend.position="right")
ggsave("figs/supplemental/mrna_rare_alleles_genic_features_distributions.pdf", plot=all_density_fig, width=12, height=3)

# Plot only 5' UTR
utr_density = ggplot(mapping=aes(Abundance, y=..density..)) +
  geom_density(mapping=aes(color="Yes", fill="Yes", alpha="Yes"), data=rarect %>% filter(Any_rare, Feature == "5' UTR"), adjust=adj, bw=bw, n=1024) +
  geom_density(lwd=.5, mapping=aes(color="No", fill="No", alpha="No"), data=no_rare, bw=bw, adjust=adj, n=1024) +
  # facet_wrap(~Feature_factor, nrow=1) +
  coord_cartesian(xlim=c(-1.5, 1.5)) +
  # labs(title=sprintf("MAF < %0.3f", MAF_limit)) +
  labs(x="mRNA Abundance", y="Density") +
  # geom_text(aes(label=sprintf("p = %1.2e \nn Rare = %i ", p_ks, nObs)), fontface="italic", color="gray30",
  #           x=Inf, y=Inf,data=sig %>% filter(Feature == "5' UTR"), inherit.aes=FALSE, hjust=1, vjust=1.1) + 
  theme_classic() +
  theme(strip.text.x = element_text(size = 12)) +
  scale_fill_manual(name="Gene has\nrare allele", values=c("No"="#03396c", "Yes"="#6497b1")) +
  scale_alpha_manual(name="Gene has\nrare allele", values=c("No"=0.2, "Yes"=1)) +
  scale_color_manual(name="Gene has\nrare allele", values=c("No"="gray", "Yes"="#6497b1")) +
  theme(legend.position=c(.8, 1), legend.justification = c(0,1))

# Plot KS D statistic
gene_colors = c("Promoter"="#C9C9C9", "5' UTR" = "#6497b1", "Introns" = "#595959", "CDS" = "#005b96", "3' UTR" = "#03396c")
# ks_fig = ggplot(sig %>% filter(Feature != "Transcript"), aes(Feature_factor, KS_D, fill=Feature_factor)) +
#   geom_col(width=0.75) +
#   geom_text(aes(label=sprintf("p = %1.2e \nn Rare = %i ", p_ks, nObs)), fontface="italic", color="gray30",
#             data=sig, hjust=0.5, vjust=-.25) +
#   labs(y="Test statistic (K-S 'D')") +
#   theme_classic() +
#   theme(axis.title.x = element_blank(),
#         axis.text.x = element_text(size=12),
#         legend.position = "none") +
#   coord_cartesian(ylim=c(0, .1)) +
#   scale_fill_manual(values=gene_colors)
# ggsave("figs/ks_d_MGC.png", width=6.6, height=2.25, dpi=600)

#P-value fig
p_data = sig %>% 
  pivot_longer(cols=c(p_ks, p_w, p_fvar), names_to = "Test", values_to = "p") %>%
  mutate(log_p = -log10(p),
         Test = case_when(Test == "p_ks" ~ "Kolmogorov-Smirnov",
                          Test == "p_w" ~ "Mann-Whitney",
                          Test == "p_fvar" ~ "Equal Variance (F)"))
p_fig = ggplot(p_data, aes(Feature_factor, log_p, color=Feature_factor, shape=Test)) +
  geom_point(position=position_dodge(width=0.2), size=3) +
  geom_text(aes(Feature_factor, y=0, label=sprintf("n Rare = %i ", nObs)), fontface="italic", color="gray30",
            data=sig, hjust=0.5, vjust=4.5, inherit.aes = FALSE) +
  theme_classic() +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_text(size=12),
        legend.position = c(.8, 1), legend.justification = c(0,1)) +
  scale_color_manual(values=gene_colors) +
  labs(y=expression(-log[10](p-value))) +
  ylim(0,8) +
  guides(color="none") +
  coord_cartesian(clip="off")

gene_cartoon = tibble(x = c(0, 0, 1, 1, 
                            1, 1, 2, 2.1, 2, 
                            2, 2, 2.5, 2.6, 2.5,
                            2.5, 2.5, 3.5, 3.5,
                            3.5, 3.5, 4.5, 4.6, 4.5,
                            4.5, 4.5, 5, 5,
                            5, 5, 5.3, 5.4, 5.3,
                            5.3, 5.3, 6, 6.1, 6),
                      y = c(-.1, .1, .1, -.1, 
                            -.25, .25, .25, 0, -.25, 
                            -.75, .75, .75, 0, -.75,
                            -.1, .1, .1, -.1,
                            -.75, .75, .75, 0, -.75,
                            -.1, .1, .1, -.1,
                            -.75, .75, .75, 0, -.75,
                            -.25, .25, .25, 0, -.25),
                      Feature = c(rep("Promoter", 4), rep("5' UTR", 5), rep("CDS", 5), rep("Intron", 4), rep("CDS", 5), rep("Intron", 4), rep("CDS", 5), rep("3' UTR", 5))) %>%
  mutate(Feature = factor(Feature, levels = c("Promoter", "3' UTR", "Intron", "CDS", "5' UTR")))
feature_labels = tibble(x=c(.5, 1.5, 2.25, 3, 4, 4.8, 5.15, 5.65), 
                        y=c(-.25, 0, 0, -.25, 0, -.25, 0, 0),
                        color=c("#C9C9C9", "white", "white", "#595959", "white", "#595959", "white", "white"),
                Feature = c("2kbp Promoter", "5' UTR", "CDS", "Intron", "CDS", "Intron", "CDS", "3' UTR"))
gene_cartoon_fig = ggplot(gene_cartoon, aes(x, y, fill=recode(Feature, Intron="Introns"), group=Feature)) +
  geom_polygon() +
  geom_text(aes(x,y, label=Feature), color=feature_labels$color, inherit.aes = FALSE, data=feature_labels, fontface="bold") +
  theme_void() +
  theme(axis.title.y = element_blank()) +
  scale_fill_manual(values=gene_colors) +
  coord_cartesian(xlim=c(0.25, 5.9)) +
  theme(legend.position = "none") 

# Make seq logo for kozak
txdb <- makeTxDbFromGFF(gff_path, format="gff3")
ref <- FaFile(ref_path)
cds = loadRegion(txdb, "cds") %>%
  unlist() %>%
  as_tibble() %>%
  filter(exon_rank == 1)
start_region = tibble(
  seqnames = cds$seqnames,
  strand = cds$strand,
  start = ifelse(strand == "+",
                 cds$start - 5,
                 cds$end - 5),
  end = ifelse(strand == "+",
               cds$start + 5,
               cds$end + 5)
)
cds_start_seqs = getSeq(ref, makeGRangesFromDataFrame(start_region))
getPwmFromAlignment(cds_start_seqs)
pwm = getPwmFromAlignment(cds_start_seqs)[,3:9]
# write_rds(pwm, "data/empirical_B73_CDS_kozak.rds")
# pwm_logo = ggseqlogo(pwm, method="prob") +
#   scale_x_discrete(limits = c("-3", "-2", "-1", "1", "2", "3", "4")) +
#   labs(x="Position")
# ggsave("figs/supplemental/kozak_pwm.pdf", pwm_logo, width=5, height=2)

# Assemble Figure 1
fig1 = p_fig + gene_cartoon_fig + (utr_density) +
  plot_layout(ncol=1, nrow=3, heights = c(4, 1, 2)) +
  plot_annotation(tag_levels = "a") &
  theme(plot.tag = element_text(face = "bold"))
ggsave("figs/supplemental/mrna_rare_alleles_genic_features.pdf", plot=fig1, width=9, height=6)

# # Top dysregulated genes ------------------------------------------------
# # Make gene conversion table
# orig_data <- read_tsv(orig_data_path) %>%
#   dplyr::select(Uniprot_Accession, Ensembl_GID)
# mapping_table <- data.table::fread(uniparc_id_path,
#                                    header=FALSE, data.table = FALSE, stringsAsFactors = FALSE) %>%
#   set_colnames(c("Uniprot_Accession", "Uniparc_Accession"))
# conversion <- data.table::fread(blast_results_path,
#                                 stringsAsFactors = FALSE, data.table = FALSE)
# gene_conversion <- conversion %>%                                               
#   dplyr::rename("Uniparc_Accession"="V1",
#                 "Protein_ID"="V2",
#                 "Coverage"="V13",
#                 "Identity"="V3") %>%
#   dplyr::select("Uniparc_Accession", "Protein_ID", "Coverage", "Identity") %>%
#   filter(Identity > 90 & Coverage > 90) %>%             # Keep only matches with 90% coverage and identity
#   mutate(Gene = sub("_P[0-9]{3}", "", Protein_ID)) %>%     # Get gene names from protein IDs
#   group_by(Gene) %>%
#   slice_head(n=1) %>%                                     # Some Genes are still duplicated. Just pick the first entry for them.
#   left_join(mapping_table) %>%                          # Pull this in for the UniProt IDs needed to match proteome data
#   inner_join(orig_data)                                    # Merge in the proteome data
# 
# # Read in annotations
# ortho = read_tsv(ortho_path, col_types = 'ccccccccc')
# func = read_tsv(function_path)
# annot = inner_join(ortho, func)
# 
# # Get top genes based on median abundance of inds with rare variants
# top_genes = rarect %>%
#   filter(Any_rare) %>%
#   # filter(grepl("UTR", Feature)) %>%
#   group_by(Feature, Gene) %>%
#   summarise(Abundance_diff = median(Abundance) - no_rare %>% filter(Gene == Gene[1]) %>% pull(Abundance) %>% median) %>%
#   # ggplot(aes(abs(Abundance))) + geom_histogram() + facet_wrap(~Feature)
#   group_by(Feature) %>%
#   top_n(abs(Abundance), n=10) %>%
#   arrange(Feature, -abs(Abundance)) %>%
#   ungroup() %>%
#   left_join(gene_conversion, by="Gene") %>%
#   left_join(annot, by=c("Ensembl_GID" = "Gene stable ID")) %>%
#   dplyr::select(Feature, Gene, Abundance, contains("Arabidopsis"), contains("Oryza"), contains("Gene")) %>%
#   unique() %>%
#   write_csv("protein_dysregulation_top10_genes.csv")


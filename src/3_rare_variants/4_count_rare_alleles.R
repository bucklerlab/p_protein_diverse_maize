# Joe Gage
# Run on CBSU large-memory machine

# Setup -------------------------------------------------------------------
setwd("/workdir/jlg374/")

# Only run if rTASSEL not installed:
# devtools::install_bitbucket(
#   repo = "bucklerlab/rTASSEL",
#   ref = "master",
#   build_vignettes = FALSE)
options(java.parameters = c("-Xmx500g"))
library(rTASSEL)
rTASSEL::startLogger(fullPath = NULL, fileName = NULL)

# * Import packages ----
library(tidyverse)
library(magrittr)
library(patchwork)
library(GenomicRanges)
library(GenomicFeatures)
library(SummarizedExperiment)

# * Set path variables ----
# These were created in src/3_rare_variants/call_hmp321_positions and 3_subset_rare_SNPs.sh
vcf_path = "/workdir/jlg374/prep_jiang_vcfs/Jiang2019_samples_chrALL_hmp321_LLD_noNI5_noIndels_infoFilled_JiangInbreds_segregatingHomozygousSites_MAF0.1.vcf.gz"
af_path = "/workdir/jlg374/uplift_hapmap321/hapmap_v5/sort_vcfs/allele_freqs/hmp321_agpv5_chrALL_LLD_noNI5_noIndels_sorted_alleleFreqs.txt"

# GFF from https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3.gz
gff_path = "data/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.gff3"

# Abundance from 2_subset_lines_format_abundance.R
abundance_path = "data/Jiang2019_AllGenos_logProtein.txt"

out_file = "data/rare_variants.txt"

# * Set parameter variables ----
chroms = 1:10
MAF_limit = 0.02
promoter_bp_upstream = 2000
promoter_bp_downstream = 0


# * Functions -------------------------------------------------------------
subsetSNPs <- function(snp_set, genomic_features, genomic_feature_name){
  # snp_set:              GRanges object containing the larger set of SNPs to be filtered
  # genomic_features:     GRanges object defining the location of some genomic feature, like 
  #                       UTRs or transcripts
  # genomic_feature_name: String; name for the genomic feature. Will be added in a column to output.
  subset <- mergeByOverlaps(snp_set, genomic_features)
  subset$Feature <- genomic_feature_name
  return(subset)
}


# Read in data ----
# * Read in genotypic data ----
# Sometimes these commands fail, e.g. claim they don't have enough memory.
#  If that happens, restart the script. Sometimes it takes a few tries.
system.time(
  geno_java <- readGenotypeTableFromPath(vcf_path)
)
system.time(
  genos <- getSumExpFromGenotypeTable(geno_java)
)
# Get genotypic data from SummarizedExperiment
genos_numeric <- assay(genos)
# Make genotype names uppercase to match proteomic data
colnames(genos_numeric) = toupper(colData(genos)$Sample)

# * Read allele frequencies ----
# This is hacky - but print the first 6 columns and feed them to fread. Otherwise
#  it gets hung up on the inconsistent number of columns. It doesn't matter that I
#  truncate some rows at the 6th column - this throws away some allele frequencies for
#  3- or 4- allele sites, but those should not be present in the VCF anyway.
fread_cmd = sprintf("cat %s | awk -F '\t' '{print $1 , $2 , $3 , $4, $5, $6}'", af_path)
all_freqs = data.table::fread(cmd = fread_cmd, data.table=FALSE, stringsAsFactors=FALSE)
# Make the DF look nicer, identify the minor allele, and create a column with MAF.
# Then turn it into a GenomicRanges object.
freq <- all_freqs %>%
  dplyr::select(1:6) %>%
  set_colnames(c("Chrom", "Pos", "nAlleles", "nChr", "Freq1", "Freq2")) %>%
  mutate(Allele1 = as.character(sub("([ACGT]):.*", "\\1", Freq1)),
         Allele2 = as.character(sub("([ACGT]):.*", "\\1", Freq2)),
         Freq1 = as.numeric(sub("[ACGT]:", "", Freq1)),
         Freq2 = as.numeric(sub("[ACGT]:", "", Freq2))) %>%
  mutate(MAF=ifelse(Freq1 < Freq2, Freq1, Freq2),
         Minor_allele=ifelse(MAF == Freq1, Allele1, Allele2),
         Major_allele=ifelse(MAF == Freq1, Allele2, Allele1)) %>%
  makeGRangesFromDataFrame(start.field = "Pos",
                           end.field = "Pos",
                           keep.extra.columns = TRUE)

# Subset frequencies to only the SNPs that are in genotype data
# Then sort so the datasets match
freq = subsetByOverlaps(freq, rowRanges(genos))
freq = sortSeqlevels(freq)
freq = sort(freq)
all(start(rowRanges(genos)) == start(freq)) # This should be TRUE!

# Update genotype info to reflect number of minor alleles
ref_is_minor <- rowRanges(genos)$refAllele == freq$Minor_allele
genos_numeric[ref_is_minor, ] = abs(genos_numeric[ref_is_minor, ] - 2)
# Remove hets
genos_numeric[genos_numeric == 1] <- NA

# Identify SNPs that aren't segregating
major = rowSums(genos_numeric == 0, na.rm=TRUE) > 0
minor = rowSums(genos_numeric == 2, na.rm=TRUE) > 0
biallelic = major & minor # We will use this later

# Compile info (MAF, GERP, etc) for the SNPs in 'genos'
snps = rowRanges(genos)


# * Add MAF to snps -------------------------------------------------------
maf_overlap = findOverlaps(freq, snps)
snps$MAF = NA
snps$Minor_allele = NA
snps$Biallelic = biallelic
snps$MAF[subjectHits(maf_overlap)] = freq$MAF[queryHits(maf_overlap)]
snps$Minor_allele[subjectHits(maf_overlap)] = freq$Minor_allele[queryHits(maf_overlap)]
snps$Major_allele[subjectHits(maf_overlap)] = freq$Major_allele[queryHits(maf_overlap)]


# Create genomic features -------------------------------------------------
txdb <- makeTxDbFromGFF(gff_path, format="gff3")
seqlevels(txdb) <- sub("chr", "", seqlevels(txdb))

# Promoters
pro <- promoters(txdb, upstream = promoter_bp_upstream, downstream = promoter_bp_downstream, columns=c("GENEID", "TXNAME"))
pro <- pro[seqnames(pro) %in% chroms, ]

# 5' UTRs
utr5 <- unlist(fiveUTRsByTranscript(txdb))
utr5$GENEID = sub("_T[0-9]{3}\\.exon\\.[0-9]{1,2}", "", utr5$exon_name)
utr5$TX = sub("\\.exon\\.[0-9]{1,2}", "", utr5$exon_name)
utr5 <- utr5[seqnames(utr5) %in% chroms, ]

# 3' UTRs
utr3 <- unlist(threeUTRsByTranscript(txdb))
utr3$GENEID = sub("_T[0-9]{3}\\.exon\\.[0-9]{1,2}", "", utr3$exon_name)
utr3$TX = sub("\\.exon\\.[0-9]{1,2}", "", utr3$exon_name)
utr3 <- utr3[seqnames(utr3) %in% chroms, ]

# Coding sequence
cds <- cds(txdb, columns=c("exon_id", "exon_name", "GENEID", "TXNAME"))
cds <- cds[seqnames(cds) %in% chroms, ]

# Introns
intr = unlist(intronsByTranscript(txdb, use.names=TRUE))
intr$GENEID=sub("(.*)_T[0-9]{3}", "\\1", names(intr))
intr <- intr[seqnames(intr) %in% chroms, ]

# Subset SNPs -------------------------------------------------------------
# Subset SNPs and genomic features based on MAF and GERP filters
# Count overlaps between SNPs and Promoters/UTRs/CDS

# -99 means to ignore GERP values - and to include SNPs with NA values for GERP cols
keep_snps <- (snps$MAF < 0.1) &    # Keep SNPs based on a liberal MAF, filter them to MAF_limit later
             (!is.na(snps$MAF)) &
             (snps$Biallelic == TRUE)

snps_filtered <- snps[keep_snps, ]
snps_filtered$Chrom = as.numeric(seqnames(snps_filtered))
snps_filtered$Pos = start(snps_filtered)
# Change the strand on snps_filtered to *. If it is '+', only merges with the positive strand genes!
strand(snps_filtered) = "*"

pro_snps <- subsetSNPs(snps_filtered, pro, "Promoter")
utr5_snps <- subsetSNPs(snps_filtered, utr5, "5' UTR")
utr3_snps <- subsetSNPs(snps_filtered, utr3, "3' UTR")
intr_snps <- subsetSNPs(snps_filtered, intr, "Intron")
cds_snps <- subsetSNPs(snps_filtered, cds, "CDS")

# Combine all features into one dataframe and get rid of redundant rows
keep_cols = c("Chrom", "Pos", "tasselIndex", "MAF", "Minor_allele", "Major_allele", "GENEID", "Feature")
comparison_snps <- rbind(pro_snps[,keep_cols], 
                         utr5_snps[,keep_cols], 
                         utr3_snps[,keep_cols], 
                         intr_snps[,keep_cols],
                         cds_snps[,keep_cols]) %>%
  as.data.frame() %>%
  mutate(Gene=unlist(GENEID)) %>%
  dplyr::select(-GENEID) %>%
  # Remove duplicates - can happen due to being in multiple transcripts
  dplyr::filter(!duplicated(.[,c("tasselIndex", "Gene", "Feature")])) 

# Read & process abundance data -------------------------------------------
abundance_ind <- data.table::fread(abundance_path, data.table=FALSE, stringsAsFactors = FALSE) %>%
  filter(!Inbred %in% c("CIMBL12", "CIMBL157", "CIMBL55")) # These inbreds don't have genotype data

# Get all genes with rare variants in the features of interest
rare <- abundance_ind %>% 
  left_join(comparison_snps) %>% 
  filter(!is.na(tasselIndex)) %>%
  as_tibble()
# Determine which Inbreds have the rare allele at each SNP of interest
#  based on their genotyp (2 == rare)
minor_allele = genos_numeric %>%
  as_tibble() %>%
  mutate(tasselIndex = rowData(genos)$tasselIndex) %>%
  # Filter to just the rare variants we are interested in
  filter(tasselIndex %in% unique(rare$tasselIndex)) %>%
  pivot_longer(!tasselIndex, names_to = "Inbred", values_to = "Genotype") %>%
  # Drop individuals that do not have a genotype call
  drop_na(Genotype) %>%
  # Indicate minor allele
  mutate(has_minor_allele = Genotype == 2)

rare %<>%
  left_join(minor_allele, by=c("Inbred", "tasselIndex")) %>%
  dplyr::select(-Genotype) %>%
  drop_na(has_minor_allele) # Get rid of observations where inbred doesn't have a genotype call

write_tsv(rare, out_file)

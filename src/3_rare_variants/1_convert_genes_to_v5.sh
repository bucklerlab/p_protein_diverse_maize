# This needs to be run line by line not as a bash script - you need to stop in the middle to
#  submit a file to uniprot.org and download the result.

cd ~/projects/p_protein_diverse_maize/

# This is from https://www.mcponline.org/cms/10.1074/mcp.RA118.001021/attachment/d7eb3324-8587-4fbb-ae68-51da648e50e1/mmc1.zip
DATA=data/convert_Jiang2019_v5/Jiang2019_139955_1_supp_230491_phrhk9.txt

cut -f1 $DATA | tail -n +2 > data/convert_Jiang2019_v5/Jiang2019_Uniprot_IDs.txt

# Not all of the above UniProt IDs are in UniProt anymore! Some have been archived.
# So they need to be converted to UniParc IDs in order to get their fastas.
# I used Jiang2019_Uniprot_IDs.txt as input on this page https://www.uniprot.org/uploadlists/
# With From = "UniProtKb AC/ID" and To = "UniParc"
# Then Downloaded all in format 'Mapping Table'

tail -n +2 ~/Downloads/uniparc-yourlist_M20210303A94466D2655679D1FD8953E075198DA80E677D8.tab > data/convert_Jiang2019_v5/Jiang2019_Uniparc_IDs.txt

# Use the uniparc IDs to get fasta sequence for each gene
touch data/convert_Jiang2019_v5/Jiang2019_Uniprot_sequences.fasta
while read -r trash ID
do
  echo $ID
  curl https://www.uniprot.org/uniparc/$ID.fasta >> data/convert_Jiang2019_v5/Jiang2019_Uniprot_sequences.fasta
done < data/convert_Jiang2019_v5/Jiang2019_Uniparc_IDs.txt

# Download v5 proteome
PROT_URL=https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0_Zm00001eb.1.protein.fa.gz
DB=data/$( basename $PROT_URL | sed 's/.gz//')
curl $PROT_URL | bgzip -d > $DB

# Make blast db
makeblastdb -in $DB -dbtype prot

# Run blastp
QUERY=data/convert_Jiang2019_v5/Jiang2019_Uniprot_sequences.fasta
OUT=data/convert_Jiang2019_v5/Jiang2019_Uniprot_sequences_v5.blastp
blastp -query $QUERY -db $DB -max_target_seqs 1 -out $OUT -outfmt "6 std qcovs"

#!/bin/bash

# This script was written to call HapMap321 SNPs from BAMs.
# supergeno.pl itself was written by Robert Bukowski and is available at:
#  https://bitbucket.org/bukowski1/maize_hapmap3_code/src/master/raw_genos/

# The results from this test (on a small number of BAMs) will be compared to
#  the SNPs from calling HapMap321 positions from GVCFs of the same samples
#  created by Arun Seetharam

# BAMs are partially on this computer and partially on another. Get copies of
#  them all into this directory.
# cp /workdir/jlg374/BAMs/*.bam ./bam/
# Copy only the complete BAMs, ignoring the tmp files made by onboing bwa-mem
#  commands:
# scp jlg374@cbsulm13.tc.cornell.edu:/workdir/jlg374/BAMs/SRR8906956.bam ./bam/
# scp jlg374@cbsulm13.tc.cornell.edu:/workdir/jlg374/BAMs/SRR8907024.bam ./bam/
# scp jlg374@cbsulm13.tc.cornell.edu:/workdir/jlg374/BAMs/SRR8907069.bam ./bam/

# Clone the repo with R.B.'s SNP calling code
git clone git@bitbucket.org:bukowski1/maize_hapmap3_code.git

# Start preparing files for calling SNPs:
#########################################

# 1 - Prepare the list of taxa (such as samples_tst). The taxa names should not
#     contain underscores "_".
# 2 - Collect the BAM files (and the corresponding *.bam.bai files) in a local
#     work directory. BAM file names are of a general form TAXON_something.bam.
#     There may be multiple BAM files for each TAXON; the genotyping program will
#     compute colective allele depths for each TAXON over all its BAM files.

touch taxa_list.txt
BAMDIR=/workdir/jlg374/BAMs
for BAM in $( ls $BAMDIR/*.bam )
do
  SRR=${BAM#"$BAMDIR/"}
  SRR=${SRR%".bam"}
  # Find the SRR name in the SRA run table, column 11 has the inbred name:
  NAME=$( grep $SRR Jiang2019_SRA_Run_Table.txt | awk -F, '{print $11}' )
  echo $NAME >> taxa_list.txt
  # Rename the bam file according to (2) above:
  NEWBAM=bam/${NAME}_${SRR}.bam
  cp $BAM $NEWBAM &
done
wait
for NEWBAM in $( ls bam/*bam )
do
  # Index the BAM file
  samtools index $NEWBAM &
done

# 3 - Ceate a local work directory where the genotyping program is going to run.
#     (for clarity, it should be different than the BAM file directory). Copy the
#     following files from this directory to the new local work directory:
#     params.xml supergeno.pl pileup_to_h5.sh
mkdir -p genotyping
cp maize_hapmap3_code/raw_genos/* ./genotyping

# 4 -In files supergeno.pl and pileup_to_h5.sh, adjust the variables $BINDIR and
#    BINDIR, respectively, to point to the root of this software is located (i.e.,
#    the directory containing the subfolder java_code).
# **DO ^THIS^ MANUALLY** Also:
#  In pileup_to_h5.sh:
#  - Change SAMPFILE to /workdir/jlg374/compare_snp_methods/taxa_list.txt
#  - Change BAMDIR to /workdir/jlg374/compare_snp_methods/bam
#  - Change H5ROOT to /workdir/jlg374/compare_snp_methods/h5
#  - Change Xmx in line 27 to 160G
#  In supergeno.pl:
#  - Change $sampfile = "/workdir/jlg374/compare_snp_methods/taxa_list.txt";
#  - Change $posfile = "/workdir/jlg374/compare_snp_methods/positions/hmp321_positions_v5_${chr}.txt";
#  - Change $h5dir = "/workdir/jlg374/compare_snp_methods/bam";
#  - Change $ioproc = 50;
#  - Change Xmx in line 39 to 160G
# The reference geome fasta file and the corresponding index (fai) file should
#    also be available in this genotyping directory (for example, maize3.fa and
#    maize3.fa.fai). If these files are kept in a separate (but still local!)
#    directory, symliks to them may be made in the genotyping directory.
# TODO: make the script get the reference by ftp from maizegdb
ln -s /workdir/jlg374/Zm-B73-REFERENCE-NAM-5.0.fa ./genotyping/Zm-B73-REFERENCE-NAM-5.0.fa
samtools faidx genotyping/Zm-B73-REFERENCE-NAM-5.0.fa

# 5 - Prepare files, one per chromosome, containing the posotions and ALT
#     alleles for sites on which genotyping is to take place. Name the files after
#     chromosomes, e.g. positions_c9_hmp321. The files consiste of two,
#     TAB-separated columns: position on the chromosome, and ALT allele. Multiple
#     ALT alleles are separated by ",", insertions are denoted by "I" and deletions:
#     by "D".
mkdir -p ./positions
for CHR in $( seq 10 )
do
  # NOTE: this are local right now.  If running this again on some other
  #  machine, copy them in first from blfs1:/data1/users/jlg374/hapmap321_v5/
  VCF=/workdir/jlg374/uplift_hapmap321/hapmap_v5/sort_vcfs/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted.vcf.gz
  OUT=positions/hmp321_positions_v5_chr${CHR}.txt
  gzip -dc $VCF | grep -vF '#' | awk '{ print $2 "\t" $5 }' > $OUT &
done

# 6 - In the file params.xml, set <output_all_sites> to true.
#     Parameters after <e> will not matter in this run.
# **DO ^THIS^ MANUALLY**
# Also:
#  - Change the reference in the mpileup command to Zm-B73-REFERENCE-NAM-5.0.fa
#  - Change gversion to '6'
#  - Change ref_h5_file to Zm-B73-REFERENCE-NAM-5.0.fa (? not sure if this is right)

# 7 - Launch the script for one chromosome, e.g.,
#     ./supergeno.pl chr10 >& supergeno_chr10.log &
#     Note - also have to change the Java version to Java8:
cd genotyping
export JAVA_HOME=/usr/local/jdk1.8.0_121
export PATH=$JAVA_HOME/bin:$PATH
# Do one chrom at a time due to memory issues
for CHR in $( seq 10 )
do
  ./supergeno.pl chr${CHR} > supergeno_chr${CHR}.log
done

# Format the outputs as VCFs
cd ..
mkdir -p out
for CHR in $( seq 10 )
do
  echo "Starting $CHR"
  FILE=genotyping/thr.all_cchr${CHR}_bqc10_q30_bwamem_tst
  OUTVCF=out/Jiang2019_samples_chr${CHR}_hmp321_LLD_noNI5_noIndels.vcf
  # Make header
  cat vcf_header.txt > $OUTVCF
  head -1 $FILE | sed 's/CHR/CHROM/' | sed 's/POS/POS\tID/' | sed 's/ALT/ALT\tQUAL\tFILTER\tINFO\tFORMAT/' >> $OUTVCF
  # Add the needed columns ID, QUAL, FILTER, and INFO with awk
  # Remove 'chr' from the chromosome column with sed
  awk 'BEGIN {OFS="\t"} NR > 1{$2=$2"\t.";  $4=$4"\t.\t.\t.\tGT:AD:GL"; print} ' $FILE | sed 's/^chr//' >> $OUTVCF
  bgzip $OUTVCF && tabix -p vcf $OUTVCF.gz
  INFOVCF=${OUTVCF%%.vcf}_infoFilled.vcf.gz
  bcftools +fill-tags $OUTVCF.gz -Oz -o $INFOVCF && tabix -p vcf $INFOVCF
done

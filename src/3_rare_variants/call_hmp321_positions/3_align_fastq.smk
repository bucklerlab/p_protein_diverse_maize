from os.path import basename

# Run me with:
#  snakemake -n --cores all -p -s 3_align_fastq.smk (Dry Run)
#  snakemake --cores all -p -s 3_align_fastq.smk

# You will need to have these two files:
#  Jiang2019_SRR_numbers.txt: One SRR ID per line, for the SRRs to download
#  Jiang2019_ReadGroup_IDs.txt: One line per SRR, with one column containing the
#    SRR ID, and a second column containing the read group info in a format like:
#    '@RG\\tID:someRG\\tSM:someSAMP\\tLB:someLIB\\tPL:ILLUMINA'

# This 'PART' business was so I could split downloading/aligning across
#  multiple machines, e.g. with Jiang_SRR_numbers_1.txt,
#  Jiang_SRR_numbers_2.txt, etc.
PART=""
if PART == "":
  SRA_list="Jiang2019_SRR_numbers.txt"
  RGSTR_table="Jiang2019_ReadGroup_IDs.txt"
else:
  SRA_list="Jiang2019_SRR_numbers_{}.txt".format(PART)
  RGSTR_table="Jiang2019_ReadGroup_IDs_{}.txt".format(PART)

REF_URL="https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0.fa.gz"

with open(SRA_list) as f:
       SRAs = [line.split()[0] for line in f]

rule all:
  input: expand("BAMs/{SRA}.bam", SRA=SRAs)

rule get_fastq:
  output:
    fq1="FASTQs/{SRA}_1.fastq.gz",
    fq2="FASTQs/{SRA}_2.fastq.gz"
  threads: 16
  log: "logs/get_fastq/{SRA}.log"
  shell:
    '''
    fasterq-dump {wildcards.SRA} -pO ./FASTQs/ > {log}
    bgzip --threads 8 FASTQs/{wildcards.SRA}_1.fastq &
    bgzip --threads 8 FASTQs/{wildcards.SRA}_2.fastq &
    wait
    '''

rule make_index:
  output:
    ref=basename(REF_URL).replace(".gz", ""),
    index="{output.ref}.sa"
  shell:
    '''
    curl {REF_URL} | gzip -dc > {output.ref}
    bwa index {output.ref}
    '''

rule align_fastq:
  input:
    fq1="FASTQs/{SRA}_1.fastq.gz",
    fq2="FASTQs/{SRA}_2.fastq.gz",
    rgstr_table=RGSTR_table,
    index=rules.make_index.output.ref
  output:
    "BAMs/{SRA}.bam"
  params:
    bwa_cpu = 8,
    sam_cpu = 2
  threads: 10
  log:
    bwa="logs/align_fastq/{SRA}_bwamem.log",
    sam="logs/align_fastq/{SRA}_samtools.log"
  shell:
    '''
    mkdir -p BAMs
    RGSTR=$( grep {wildcards.SRA} {input.rgstr_table} | cut -f2 )
    bwa mem \
      -t {params.bwa_cpu} \
      -R $RGSTR \
      {input.index} \
      {input.fq1} \
      {input.fq2} 2> {log.bwa} |\
    samtools sort \
      -l1 \
      -m 5G \
      -@ {params.sam_cpu} \
      -o {output} 2> {log.sam}
    '''

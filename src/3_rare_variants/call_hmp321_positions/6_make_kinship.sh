# Make kinshp matrix from HapMap SNPs

GENO=./data/Jiang2019_samples_chrALL_hmp321_LLD_noNI5_noIndels_infoFilled.vcf.gz

/programs/tassel-5-standalone/run_pipeline.pl \
  -Xmx500g \
  -importGuess $GENO \
  -KinshipPlugin \
  -method Centered_IBS \
  -endPlugin \
  -export data/Jiang2019_kinship.txt \
  -exportType SqrMatrix
  
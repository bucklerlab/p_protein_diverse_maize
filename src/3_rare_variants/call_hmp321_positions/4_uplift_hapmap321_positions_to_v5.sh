#!/bin/bash

set -euxo pipefail

# Run this on a CBSU large memory gen2 machine (512Gb RAM, 96 cores)

mkdir -p /workdir/jlg374/uplift_hapmap321
cd /workdir/jlg374/uplift_hapmap321

# Get unimputed Hapmap3.2.1 from CyVerse
echo "Getting unimputed Hapmap3.2.1 on B74v4 from CyVerse"
iget -r /iplant/home/shared/panzea/hapmap3/hmp321/unimputed/uplifted_APGv4

# Filter v4 HapMap to sites that are snps or small indels, have local ld (LLD),
#  are biallelic, and
echo "Filtering Hapmap files to SNPs with LLD=True and NI5=False"
for FILE in $( ls uplifted_APGv4/*vcf.gz )
do
  VCFOUT=${FILE%%.vcf.gz}_LLD_noNI5_noIndels.vcf.gz
  bcftools view -i "LLD=1 & NI5=0" -v snps -Oz $FILE -o $VCFOUT &
done
wait

# Get SNP positions in BED-like format
echo "Getting SNP positions and converting to BED-like format for CrossMap"
for FILE in $( ls uplifted_APGv4/*_LLD_noNI5*.vcf.gz )
do
  POSOUT=$( basename $FILE | sed 's/vcf.gz/bed/' )
  gzip -dc $FILE | grep -Fv '#' | awk '{print $1 "\t" $2 "\t" $2}' > $POSOUT &
done
wait

#################################
# Uplift all hapmap from v4 to v5
#################################
echo "Starting uplift to v5..."

# Download chain file:
echo "Downloading chain file"
CHAIN_URL=https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/chain_files/B73_RefGen_v4_to_Zm-B73-REFERENCE-NAM-5.0.chain
wget $CHAIN_URL
CHAIN=$( basename $CHAIN_URL )

# Download v5 reference genome:
echo "Downloading v5 reference"
REF_URL=https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/Zm-B73-REFERENCE-NAM-5.0.fa.gz
REF=$( basename $REF_URL )
wget $REF_URL
gzip -d $REF
REF=$( basename $REF_URL | sed 's/.gz//' )
# Remove 'chr' from reference fasta - otherwise it doesn't work with the chain
#  file and VCF file (which have numeric chr designations).
sed -i 's/chr//' $REF

echo "Starting Crossmap..."
# Prep paths for CrossMap
export PYTHONPATH=/programs/CrossMap-0.3.8/lib64/python3.6/site-packages:/programs/CrossMap-0.3.8/lib/python3.6/site-packages
export PATH=/programs/CrossMap-0.3.8/bin:$PATH

# Run CrossMap
mkdir -p hapmap_v5/crossmap

for CHR in $( seq 10 )
do
  BEDIN=hmp321_agpv4_chr${CHR}_LLD_noNI5_noIndels.bed
  BEDOUT=hapmap_v5/crossmap/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_positions.txt
  VCFIN=uplifted_APGv4/hmp321_agpv4_chr${CHR}_LLD_noNI5_noIndels.vcf.gz
  VCFOUT=hapmap_v5/crossmap/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels.vcf

  CrossMap.py bed $CHAIN $BEDIN > $BEDOUT &
  # This doesn't work because of <INS> and <DEL>
  CrossMap.py vcf $CHAIN $VCFIN $REF $VCFOUT &

done
wait
echo "Uplifting complete."

#################################
# Redistribute SNPs to proper chromosomes and sort
#################################
echo "Sorting uplifted SNPs..."
cd hapmap_v5
mkdir -p sort_vcfs
cd sort_vcfs

# Merge all chroms together
# This is probably unnecessary and could be done more efficiently
#  by passing through each vcf and sending lines to their own respective
#  chromosomal vcf.
touch merged.vcf
for CHR in $( seq 10 )
do
  cat ../../hapmap_v5/crossmap/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels.vcf >> merged.vcf
done

# Then for each chromosome CHR:
# 1 - Put the header in a file
# 2 - from the merged file, pull lines starting with CHR
# 3 - sort those lines and append them to the file containing the header

for CHR in $( seq 10 )
do
  mkdir -p tmp_sort_$CHR
  SORTED=hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted.vcf
  grep -F "#" ../../hapmap_v5/crossmap/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels.vcf > $SORTED
  grep -P "^${CHR}\t" merged.vcf |\
  sort -n -k2 --parallel 10 -S 19% -T ./tmp_sort_$CHR/ >> $SORTED &
  echo "${CHR} Launched"
  # Only do half the chroms at a time
  if [[ $CHR -eq 5 ]]
  then
    wait
  fi
done
wait
rm merged.vcf
rm -r tmp_sort_*
echo "Done sorting."

# Compress and index
echo "Compressing and indexing uplifted VCFs..."
for CHR in $( seq 10 )
do
  FILE=hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted.vcf
  bgzip -c $FILE > $FILE.gz &
done
wait

for CHR in $( seq 10 )
do
  FILE=hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted.vcf
  tabix -p vcf $FILE.gz &
done
wait
echo "Done."

# TODO: Pull MAF for all sites
echo "Getting MAF for all sites"
mkdir allele_freqs
for FILE in hmp321_agpv5_chr*_LLD_noNI5_noIndels_sorted.vcf.gz
do
  OUT=allele_freqs/${FILE%%.vcf.gz}_alleleFreqs
  vcftools --gzvcf $FILE --freq --out $OUT &
done
wait

# Concatenate all allele frequency files into one
ALL_FRQ=allele_freqs/hmp321_agpv5_chrALL_LLD_noNI5_noIndels_sorted_alleleFreqs.txt
for CHR in $( seq 10 )
do
  FRQ=allele_freqs/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted_alleleFreqs.frq
  if [ $CHR -eq 1 ]
  then
    cat $FRQ > $ALL_FRQ
  else
    tail -n +2 $FRQ >> $ALL_FRQ
  fi
done

echo "Done"

echo "All Done."

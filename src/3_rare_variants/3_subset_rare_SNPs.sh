#!/bin/bash

set -euxo pipefail

# Run this on a CBSU machine

MAF=0.1

mkdir -p /workdir/jlg374/prep_jiang_vcfs

# Get unimputed Hapmap3.2.1 on v5
HMPV5_DIR=/workdir/jlg374/uplift_hapmap321/hapmap_v5/sort_vcfs/

# Filter all files on MAF
for FILE in $( ls ${HMPV5_DIR}/*vcf.gz )
do
  OUT=prep_jiang_vcfs/$( basename ${FILE%%.vcf.gz}_MAF${MAF}.vcf )
  bcftools view -i "LLD=1 & NI5=0 & MAF<=${MAF}" -Ov -o $OUT $FILE &
done
wait

###########################
# Filter sites and samples
###########################

# Take the sites identified above as having rare alleles, and pull
#  those sites from the Jiang VCFs.
# Then filter to only markers that are biallelic and have no hets.
# This should leave only rare alleles that are segregating in these individuals.
for CHR in $( seq 10 )
do
  echo "Starting $CHR"
  HMP=prep_jiang_vcfs/hmp321_agpv5_chr${CHR}_LLD_noNI5_noIndels_sorted_MAF${MAF}.vcf
  JIANG=compare_snp_methods/out/Jiang2019_samples_chr${CHR}_hmp321_LLD_noNI5_noIndels_infoFilled.vcf.gz

  # Get rare variant site positions from hapmap file\
  POS=prep_jiang_vcfs/hmp_MAF${MAF}_positions_chr${CHR}.txt
  grep -Fv "#" $HMP | cut -f1,2 > $POS

  OUT=prep_jiang_vcfs/$( basename ${JIANG%%.vcf.gz}_JiangInbreds_segregatingHomozygousSites_MAF${MAF}.vcf.gz )
  bcftools view -T $POS --min-ac 2 -m2 -M2 -v snps $JIANG -Oz -o $OUT &
done
wait

# Concatenate the chromosomes
CONCAT_FILES=""
# ALL_FRQ=allele_freq_LLD_noNI5_MAF${MAF}_4Inbreds_segregatingHomozygousSites.txt
for CHR in $( seq 10 )
do
  # Construct string for concatenating vcfs with bcftools
  FILE=prep_jiang_vcfs/Jiang2019_samples_chr${CHR}_hmp321_LLD_noNI5_noIndels_infoFilled_JiangInbreds_segregatingHomozygousSites_MAF${MAF}.vcf.gz
  bcftools index $FILE
  CONCAT_FILES="$CONCAT_FILES $FILE"
done

bcftools concat -a $CONCAT_FILES -Oz -o prep_jiang_vcfs/Jiang2019_samples_chrALL_hmp321_LLD_noNI5_noIndels_infoFilled_JiangInbreds_segregatingHomozygousSites_MAF${MAF}.vcf.gz
tabix -p vcf prep_jiang_vcfs/Jiang2019_samples_chrALL_hmp321_LLD_noNI5_noIndels_infoFilled_JiangInbreds_segregatingHomozygousSites_MAF${MAF}.vcf.gz

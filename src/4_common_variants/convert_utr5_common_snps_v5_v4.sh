#!/bin/bash

# Run on cbsu machin

export PYTHONPATH=/programs/CrossMap-0.3.8/lib64/python3.6/site-packages:/programs/CrossMap-0.3.8/lib/python3.6/site-packages
export PATH=/programs/CrossMap-0.3.8/bin:$PATH

wget   https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/chain_files/Zm-B73-REFERENCE-NAM-5.0_to_B73_RefGen_v4.chain

CrossMap.py bed \
  Zm-B73-REFERENCE-NAM-5.0_to_B73_RefGen_v4.chain \
  utr5_common_snps_v5.bed \
  utr5_common_snps_v4.bed
  
# Note - all but 5 SNPs mapped to v4 successfully
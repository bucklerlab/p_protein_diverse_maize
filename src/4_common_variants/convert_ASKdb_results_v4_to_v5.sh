#!/bin/bash

# Run on CBSU machine

# Prepare CrossMap
export PYTHONPATH=/programs/CrossMap-0.3.8/lib64/python3.6/site-packages:/programs/CrossMap-0.3.8/lib/python3.6/site-packages
export PATH=/programs/CrossMap-0.3.8/bin:$PATH

wget https://download.maizegdb.org/Zm-B73-REFERENCE-NAM-5.0/chain_files/B73_RefGen_v4_to_Zm-B73-REFERENCE-NAM-5.0.chain

# Make bed file from ASKdb results
RESULTS=data/res_all.csv 
awk -F "," '{print $1"\t"$2-1"\t"$2-1"\t"$3"\t"$4}' $RESULTS | tail -n +2 > ${RESULTS%%.csv}.bed

CrossMap.py bed \
  B73_RefGen_v4_to_Zm-B73-REFERENCE-NAM-5.0.chain \
  ${RESULTS%%.csv}.bed \
  data/utr5_common_snps_GWAS_v5.bed